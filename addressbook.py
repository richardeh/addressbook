"""
Addressbook.py
Manages a database of individuals and related information about them

Author: Richard Harrington
Date Created: 9/27/2013
Last Updated: 10/18/2013
"""

# Schema for addressbook.db

# Table: people
# Columns: ID (autoincrement),name (30), birthdate, wishlist, picture,
#           facebook, twitter

# Table: addresses
# Columns: ID (auto), contact_ID (Foreign Key people.ID), address (50),
#           city (25), state (2), zip (5), address_type (10)

# Table: phone
# Columns: ID (auto), contact_ID (FK people.ID), phone_number (10),
#           number_type (10)

# Table: email
# Columns: ID (auto), contact_ID (FK people.ID),email_address, email_type(10)


# Address_type, email_type and number_type should be like 'home','work','mobile', etc.

import tkinter
import sqlite3
import re

def send_sql(statement):
    # Sends an input SQL statement to the database
    
    try:
        results=[]
        con=sqlite3.connect('addressbook.db')
        c=con.cursor()
        c.execute(statement)
        for result in c.fetchall():
            results.append(result)
        con.commit()
        return results
    except sqlite3.Error as e:
        if con:
            con.rollback()
        print ("Error %s:" % e.args[0])
    finally:
        if con:
            con.close()

def add_contact():
    # Add a contact
    contact_window("add")

def delete_contact():
    # Delete a contact
    tables=("phone","addresses","email")
    name=tf_listbox.get(tf_listbox.curselection())
    statement="""select ID from people where name='"""+name+"'"
    contact_ID=str(send_sql(statement)[0][0])
    result=tkinter.messagebox.askyesno("Warning","Delete "+name+"?",icon='warning')
    if result:
        for table in tables:
            sql="delete from "+table+" where contact_ID="+contact_ID
            send_sql(sql)

    sql="delete from people where ID="+contact_ID
    send_sql(sql)
    tf_listbox.delete(tf_listbox.curselection())
    tkinter.messagebox.showinfo("Delete complete",name+" deleted. I hope you're happy.")
def update_contact():
    # Change or add information on a contact
    try:
        contact,phones,addresses,emails=get_contact()
        contact_window("update",contact,phones,addresses,emails)
    except:
        print("Please select a contact")

def view_contact():
    # Display all contact information for an individual
    try:
        contact,phones,addresses,emails=get_contact()
        contact_window("view",contact,phones,addresses,emails)
    except:
        print("Please select a contact")
    
def get_contact():
    # Retrieve all contact information for an individual
    phones={}
    addresses={}
    emails={}
    try:
        name=tf_listbox.get(tf_listbox.curselection())
        
        statement="""select ID from people where name='"""+name+"'"
        contact_ID=str(send_sql(statement)[0][0])
        
        statement="""select birthdate,wishlist,picture,facebook,twitter from people where ID="""+contact_ID
        contact_data=send_sql(statement)
        birthdate=contact_data[0][0]
        wishlist=contact_data[0][1]
        picture=contact_data[0][2]
        facebook=contact_data[0][3]
        twitter=contact_data[0][4]

        statement="""select phone_number,number_type from phone where contact_ID="""+contact_ID
        
        for row in send_sql(statement): phones[row[1]]=row[0]
        
        statement="""select email_address,email_type from email where contact_ID="""+contact_ID
        for row in send_sql(statement): emails[row[1]]=row[0]

        statement="""select address,city,state,zip,address_type from addresses where contact_ID="""+contact_ID
        for row in send_sql(statement): addresses[row[4]]=row[0]+", "+row[1]+", "+row[2]+", "+row[3]

        contact={'name':name,'birthdate':birthdate,'wishlist':wishlist,'picture':picture,'facebook':facebook,'twitter':twitter,'Contact_ID':contact_ID}
        return contact,phones,addresses,emails
    except:
        print(Exception)

def contact_window(operation,contact="",phones="",addresses="",emails=""):
    # Create a contact window
    contact_window=tkinter.Tk()

    a_entry=[]
    p_entry=[]
    e_entry=[]
    c_entry=[]
    a_lbl=[]
    p_lbl=[]
    e_lbl=[]
    c_lbl=[]
    
    if operation is "view":
        # Draw a window with the contact information for the selected contact
        
        contact_label=tkinter.Label(contact_window,text="Name: "+contact['name'])    
        contact_label.pack()
        for key in addresses:
            address_label=tkinter.Label(contact_window,text=key+" address: "+addresses[key])
            address_label.pack()
        for key in phones:
            phone_label=tkinter.Label(contact_window,text=key+" phone: "+phones[key])
            phone_label.pack()
        for key in emails:
            email_label=tkinter.Label(contact_window,text=key+" email: "+emails[key])
            email_label.pack()
        for key in contact:
            if contact[key] is not None and key is not 'name':
                contact_label=tkinter.Label(contact_window,text=key+": "+contact[key])
                contact_label.pack()

    elif operation is "update":
        # Draw a window with the current contact information for the selected contact
        # and the ability to modify that information and save it
        
        frame=tkinter.Frame(contact_window)
        c_lbl.append(tkinter.Label(frame,text="Name: ",justify="left"))
        c_entry.append(tkinter.Entry(frame))
        c_entry[0].insert(0,contact['name'])
        
        c_lbl[0].pack(side='left')
        c_entry[0].pack(side='right')
        frame.pack()

        for key in addresses:
           frame=tkinter.Frame(contact_window)
           a_lbl.append(tkinter.Label(frame,text=key+" address: "))
           a_entry.append(tkinter.Entry(frame))
           a_entry[len(a_entry)-1].insert(0,addresses[key])

           a_lbl[len(a_lbl)-1].pack(side='left')
           a_entry[len(a_entry)-1].pack(side='right')
           frame.pack()

        for key in phones:
            frame=tkinter.Frame(contact_window)
            p_lbl.append(tkinter.Label(frame,text=key+" phone: "))
            p_entry.append(tkinter.Entry(frame))
            p_entry[len(p_entry)-1].insert(0,phones[key])

            p_lbl[len(p_lbl)-1].pack(side='left')
            p_entry[len(p_entry)-1].pack(side='right')
            frame.pack()
        
        for key in emails:
            frame=tkinter.Frame(contact_window)
            e_lbl.append(tkinter.Label(frame,text=key+" email: "))
            e_entry.append(tkinter.Entry(frame))
            e_entry[len(e_entry)-1].insert(0,emails[key])

            e_lbl[len(e_lbl)-1].pack(side='left')
            e_entry[len(e_entry)-1].pack(side='right')
            frame.pack()

        for key in contact:
            if key is not 'name':
                frame=tkinter.Frame(contact_window)
                c_lbl.append(tkinter.Label(frame,text=key+": ",justify="left"))
                c_entry.append(tkinter.Entry(frame))
            
                if contact[key] is not None: c_entry[len(c_entry)-1].insert(0,contact[key])
                c_lbl[len(c_lbl)-1].pack(side='left')
                c_entry[len(c_entry)-1].pack(side='right')
                frame.pack()


    else:
        # Draw a window with frames and entry boxes for contact information

        # Name
        name_frame=tkinter.Frame(contact_window)
        c_lbl.append(tkinter.Label(name_frame,text="Name:"))
        c_entry.append(tkinter.Entry(name_frame))
        
        c_lbl[len(c_lbl)-1].pack(side='left')
        c_entry[len(c_entry)-1].pack(side='right')
        name_frame.pack()
            
        # Birthdate
        bday_frame=tkinter.Frame(contact_window)
        c_lbl.append(tkinter.Label(bday_frame,text="Birthdate:"))
        c_entry.append(tkinter.Entry(bday_frame))

        c_lbl[len(c_lbl)-1].pack(side='left')
        c_entry[len(c_entry)-1].pack(side='right')
        bday_frame.pack()

        # Facebook
        fb_frame=tkinter.Frame(contact_window)
        c_lbl.append(tkinter.Label(fb_frame,text="Facebook:"))
        c_entry.append(tkinter.Entry(fb_frame))

        c_lbl[len(c_lbl)-1].pack(side='left')
        c_entry[len(c_entry)-1].pack(side='right')
        fb_frame.pack()

        # Twitter
        tw_frame=tkinter.Frame(contact_window)
        c_lbl.append(tkinter.Label(tw_frame,text="Twitter:"))
        c_entry.append(tkinter.Entry(tw_frame))

        c_lbl[len(c_lbl)-1].pack(side='left')
        c_entry[len(c_entry)-1].pack(side='right')
        tw_frame.pack()

        # Wishlist
        wl_frame=tkinter.Frame(contact_window)
        c_lbl.append(tkinter.Label(wl_frame,text="Wishlist:"))
        c_entry.append(tkinter.Entry(wl_frame))

        c_lbl[len(c_lbl)-1].pack(side='left')
        c_entry[len(c_entry)-1].pack(side='right')
        wl_frame.pack()

        # Picture
        pic_frame=tkinter.Frame(contact_window)
        c_lbl.append(tkinter.Label(pic_frame,text="Picture:"))
        c_entry.append(tkinter.Entry(pic_frame))
        
        c_lbl[len(c_lbl)-1].pack(side='left')
        c_entry[len(c_entry)-1].pack(side='right')
        pic_frame.pack()

    if operation is "update" or operation is "add":
        # Buttons to save the changes, or add other contact information
        buttonframe=tkinter.Frame(contact_window)
        save_btn=tkinter.Button(buttonframe,text="Save Contact",command=lambda:save_contact(a_lbl,a_entry,c_lbl,c_entry,e_lbl,e_entry,p_lbl,p_entry,operation))
        save_btn.pack()

        address_btn=tkinter.Button(buttonframe,text="Add an Address",command=lambda:add_dialog(c_entry[0].get(),"address"))
        address_btn.pack()

        phone_btn=tkinter.Button(buttonframe,text="Add a phone number",command=lambda:add_dialog(c_entry[0].get(),"phone"))
        phone_btn.pack()

        email_btn=tkinter.Button(buttonframe,text="Add email address",command=lambda:add_dialog(c_entry[0].get(),"email"))
        email_btn.pack()
        
        buttonframe.pack()

    contact_window.title("Contact details")
    contact_window.mainloop()

def add_dialog(name,operation):
    # Add contact detail to an existing contact

    try:
        statement="select ID from people where name='"+name+"'"
        contact_ID=send_sql(statement)[0][0]
    except Exception as e:
        tkinter.messagebox.showinfo("Data error","Please save your contact before adding more information")
        return 0

    add_window=tkinter.Tk()
    
    type_frame=tkinter.Frame(add_window)
    type_lbl=tkinter.Label(type_frame,text=str(operation.capitalize())+" type:")
    type_entry=tkinter.Entry(type_frame)
    
    type_lbl.pack(side='left')
    type_entry.pack(side='right')
    type_frame.pack()

    
    data_frame=tkinter.Frame(add_window)
    data_lbl=tkinter.Label(data_frame,text=str(operation.capitalize()))
    data_entry=tkinter.Entry(data_frame)

    data_lbl.pack(side='left')
    data_entry.pack(side='right')
    data_frame.pack()
    
    if operation is not 'address':
        add_button=tkinter.Button(add_window,text="Add "+operation,command=lambda:add_info(operation,type_entry.get(),data_entry.get(),contact_ID))
        add_button.pack()
    else:
        # Need to add city, state, and zip code entries for an address
        city_frame=tkinter.Frame(add_window)
        city_lbl=tkinter.Label(city_frame,text="City: ")
        city_entry=tkinter.Entry(city_frame)
        city_lbl.pack(side='left')
        city_entry.pack(side='right')
        city_frame.pack()

        state_frame=tkinter.Frame(add_window)
        state_lbl=tkinter.Label(state_frame,text="State: ")
        state_entry=tkinter.Entry(state_frame)
        state_lbl.pack(side='left')
        state_entry.pack(side='right')
        state_frame.pack()

        zip_frame=tkinter.Frame(add_window)
        zip_lbl=tkinter.Label(zip_frame,text="Zip Code:")
        zip_entry=tkinter.Entry(zip_frame)
        zip_lbl.pack(side='left')
        zip_entry.pack(side='right')
        zip_frame.pack()
        
        add_button=tkinter.Button(add_window,text="Add "+operation,command=lambda:add_info(operation,type_entry.get(),data_entry.get(),contact_ID,city_entry.get(),state_entry.get(),zip_entry.get()))
        add_button.pack()
        
    
    add_window.title("Add "+operation.capitalize())
    add_window.mainloop()

def add_info(operation,add_type,add_data,contact_ID,city="",state="",zip_code=""):
    # Adds the contact information to the database

    # First validate the data
    if operation is "phone":
        if len(add_data) >= 10:
            add_data="".join([s for s in add_data.split("-")])
        if len(add_data) is not 10:
            tkinter.messagebox.showinfo("Data error","Please enter a ten-digit phone number")
            return False
    elif operation is "email":
        expression="^[A-Z0-9a-z._%+-]+@[A-Z0-9a-z.-]+\.(?:[A-Za-z]){2,6}$"
        if not re.match(expression,add_data):
            tkinter.messagebox.showinfo("Data error","Invalid e-mail address")
            return False
    else:
        if len(city) is 0:
            tkinter.messagebox.showinfo("Data error","Please enter a city")
            return False
        elif len(state) is not 2:
            tkinter.messagebox.showinfo("Data error","Please enter a two-letter state code")
            return False
        elif len(zip_code) is not 5:
            tkinter.messagebox.showinfo("Data error","Please enter a five-digit zip code")
            return False
    
    sql="Insert into "+operation
    if operation is "address":
        sql+="es"
        cols="contact_ID,address_type,address,city,state,zip"
        city=" ".join([e.capitalize() for e in city.split()])
        add_data=" ".join([e.capitalize() for e in add_data.split()])
        
        sql+=" ("+cols+") values ("+str(contact_ID)+",'"+add_type+"','"+add_data+"','"+city+"','"+state.upper()+"','"+str(zip_code)+"')"
    elif operation is "phone":
        cols="contact_ID,number_type,phone_number"
    else:
        cols="contact_ID,email_type,email_address"

    if operation is not "address":
        sql+=" ("+cols+") values ("+str(contact_ID)+",'"+add_type+"','"+add_data+"')"
    try:
        send_sql(sql)
    except:
        print(Exception)

def save_contact(a_lbl,a_entry,c_lbl,c_entry,e_lbl,e_entry,p_lbl,p_entry,operation):
    # Create a contact or update an existing one
    
    addresses={}
    contact={}
    emails={}
    phones={}

    for label in a_lbl:
        a_type=label.cget("text").split(":")[0].lower()
        address=a_entry[a_lbl.index(label)].get()
        addresses[a_type]=address

    for label in c_lbl:
        c_type=label.cget("text").split(":")[0].lower()
        contact_info=c_entry[c_lbl.index(label)].get()
        contact[c_type]=contact_info

    for label in e_lbl:
        e_type=label.cget("text").split(":")[0].lower()
        email=e_entry[e_lbl.index(label)].get()
        emails[e_type]=email

    for label in p_lbl:
        p_type=label.cget("text").split(":")[0].lower()
        phone=p_entry[p_lbl.index(label)].get()
        phones[p_type]=phone

    sql=""

    if operation=="add":
        # build the SQL to add a person into the database
        sql="insert into people ("
        for key in contact:
            if contact[key] is not "":
                sql+=key+","
        sql=sql[:len(sql)-1]
        sql+=") values ("
        
        for key in contact:
            if contact[key] is not "":
                sql+="'"+contact[key]+"',"
        sql=sql[:len(sql)-1]
        sql+=")"
        try:
            send_sql(sql)
            tf_listbox.insert('end',contact['name'])
        except:
            print(Exception)
        
    else:
        # First get the current record
        c,p,a,e=get_contact()
        sql="update"

        # Only update whas has actually changed
        for key in contact:
            if contact[key] != c[key]:
                c_string=" people set "+key+"='"+contact[key]+"' where name='"+c['name']+"'"
                try:
                    send_sql(sql+c_string)
                except:
                    print(Exception)

        # Verify that the input data is of the correct length
        for key in phones:
            if len(phones[key]) >= 10:
                phones[key]="".join([s for s in phones[key].split("-")])
            if len(phones[key]) is not 10:
                tkinter.messagebox.showinfo("Data error","Please enter a ten-digit phone number")
                return False
            if phones[key] != p[key]:
                p_string=" phones set "+key+"='"+phones[key]+"' where contact_ID='"+c['contact_ID']+"'"
                try:
                    send_sql(sql+p_string)
                except:
                    print(Exception)

        # Again, only transact when a change has occured
        for key in addresses:
            if addresses[key] != a[key]:
                a_string=" addresses set "+key+"='"+addresses[key]+"' where contact_ID='"+c['contact_ID']+"'"
            try:
                send_sql(sql+a_string)
            except:
                print(Exception)

        # Verify that the input data is a valid format- alpha-numeric, '@', alpha-numeric, '.' , alphabetic
        # Allows for all current top-level domains up to six characters, as well as any two-character country codes
        for key in emails:
            
            expression="^[A-Z0-9a-z._%+-]+@[A-Z0-9a-z.-]+\.(?:[A-Za-z]){2,6}$"
            if not re.match(expression,emails[key]):
                tkinter.messagebox.showinfo("Data error","Invalid e-mail address")
                return False

            if emails[key] != e[key]:
                e_string=" email set "+key+"='"+emails[key]+"' where contact_ID='"+c['contact_ID']+"'"
            try:
                send_sql(sql+e_string)
            except:
                print(Exception)
        
main_window=tkinter.Tk()
main_window.title("Address Book")

labelframe=tkinter.Frame()
textframe=tkinter.Frame()
buttonframe=tkinter.Frame()

lf_label=tkinter.Label(labelframe,anchor="n",text="Contacts")
lf_label.pack()

tf_listbox=tkinter.Listbox(textframe)
tf_listbox.pack()

# Fill the listbox with names
tf_listbox.insert(0,"")
statement="""select name from people"""
for contact in sorted(send_sql(statement)):
    tf_listbox.insert('end',contact[0])
tf_listbox.delete(0)

# Build and pack buttons for modifying the contact list
detail_button = tkinter.Button(buttonframe,text="Details",command=view_contact,width=10)
add_button = tkinter.Button(buttonframe,text="Add",command=add_contact,width=10)
delete_button = tkinter.Button(buttonframe,text="Delete",command=delete_contact,width=10)
update_button = tkinter.Button(buttonframe,text="Update",command=update_contact,width=10)

detail_button.pack()
add_button.pack()
update_button.pack()
delete_button.pack()
    
labelframe.pack(side='top')
textframe.pack(side='left')
buttonframe.pack(side='right')

main_window.mainloop()
